#!/bin/bash -i
#
# ZigzagProxy (ZPROXY)
# 
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/. 
# 
# Copyright (C) 2012
# Free Software Foundation, Inc.
# 
# For information or to collaborate on the project:
# https://savannah.nongnu.org/projects/zproxy
# 
# Gianluca Zoni
# http://inventati.org/zoninoz
# zoninoz@inventati.org
#

prog="zproxy"
proxy_list_sources=(
    https://ip-adress.com/proxy_list/
    http://proxy-list.org/en/index.php
)

PROG=$(tr a-z A-Z <<< "$prog")
path_prog="$HOME/.$prog"
path_tmp="$path_$prog"
mkdir -p "$path_prog"
#proxy_types=( Transparent )
credentials="$path_prog/.credenziali_router.txt"
max_waiting=40
    
url_update="http://git.savannah.nongnu.org/cgit/zproxy.git/snapshot/zproxy-1.0.tar.gz"

update=false
help=false
disable_proxy=false
transparent=false
anonymous=false
elite=false
reconnect=false
ip=false

user_agent="Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
user_lang="$LANG"
user_language="$LANGUAGE"
prog_lang='en_US.UTF-8:en'

ip_server_url='http://indirizzo-ip.com/ip.php'

function init_colors {
	# Reset
    Color_Off='\e[0m'       # Text Reset
    
	# Regular Colors
    Black='\e[0;30m'        # Nero
    Red='\e[0;31m'          # Rosso
    Green='\e[0;32m'        # Verde
    Yellow='\e[0;33m'       # Giallo
    Blue='\e[0;34m'         # Blu
    Purple='\e[0;35m'       # Viola
    Cyan='\e[0;36m'         # Ciano
    White='\e[0;37m'        # Bianco
    
	# Bold
    BBlack='\e[1;30m'       # Nero
    BRed='\e[1;31m'         # Rosso
    BGreen='\e[1;32m'       # Verde
    BYellow='\e[1;33m'      # Giallo
    BBlue='\e[1;34m'        # Blu
    BPurple='\e[1;35m'      # Viola
    BCyan='\e[1;36m'        # Ciano
    BWhite='\e[1;37m'       # Bianco
    
	# Underline
    UBlack='\e[4;30m'       # Nero
    URed='\e[4;31m'         # Rosso
    UGreen='\e[4;32m'       # Verde
    UYellow='\e[4;33m'      # Giallo
    UBlue='\e[4;34m'        # Blu
    UPurple='\e[4;35m'      # Viola
    UCyan='\e[4;36m'        # Ciano
    UWhite='\e[4;37m'       # Bianco
    
	# Background
    On_Black='\e[40m'       # Nero
    On_Red='\e[41m'         # Rosso
    On_Green='\e[42m'       # Verde
    On_Yellow='\e[43m'      # Giallo
    On_Blue='\e[44m'        # Blu
    On_Purple='\e[45m'      # Purple
    On_Cyan='\e[46m'        # Ciano
    On_White='\e[47m'       # Bianco
    
	# High Intensty
    IBlack='\e[0;90m'       # Nero
    IRed='\e[0;91m'         # Rosso
    IGreen='\e[0;92m'       # Verde
    IYellow='\e[0;93m'      # Giallo
    IBlue='\e[0;94m'        # Blu
    IPurple='\e[0;95m'      # Viola
    ICyan='\e[0;96m'        # Ciano
    IWhite='\e[0;97m'       # Bianco
    
	# Bold High Intensty
    BIBlack='\e[1;90m'      # Nero
    BIRed='\e[1;91m'        # Rosso
    BIGreen='\e[1;92m'      # Verde
    BIYellow='\e[1;93m'     # Giallo
    BIBlue='\e[1;94m'       # Blu
    BIPurple='\e[1;95m'     # Viola
    BICyan='\e[1;96m'       # Ciano
    BIWhite='\e[1;97m'      # Bianco
    
	# High Intensty backgrounds
    On_IBlack='\e[0;100m'   # Nero
    On_IRed='\e[0;101m'     # Rosso
    On_IGreen='\e[0;102m'   # Verde
    On_IYellow='\e[0;103m'  # Giallo
    On_IBlue='\e[0;104m'    # Blu
    On_IPurple='\e[10;95m'  # Viola
    On_ICyan='\e[0;106m'    # Ciano
    On_IWhite='\e[0;107m'   # Bianco
}


function print_c {
    case "$1" in
	1)
	    echo -n -e '\e[1;32m' #verde
	    ;;
	2)
	    echo -n -e '\e[1;33m' #giallo
	    ;;	
	3)
	    echo -n -e '\e[1;31m' #rosso
	    ;;	
	4)
	    echo -n -e "$BBlue"
    esac
    echo -n -e "$2\n"
    echo -n -e "${Color_Off}"
}

function separator {
	#COLUMNS=$( tput cols ) 2>/dev/null
    if [ -z "$COLUMNS" ]
    then 
	COLUMNS=50
    fi

    echo -ne "${BBlue}"
    for column in $(seq 1 $COLUMNS)
    do
	echo -n -e "$1"
    done
    #\e[1;34m

    echo -ne "${Color_Off}"
}


function separator- {
    separator "─"
}

function fclear {
    #echo -n -e "\e[0;37m\e[40m\ec"
    #echo -n -e "\ec\e[37m\e[40m\e[J"
    echo -n -e "\ec${White}${On_Black}\e[J"
}

function cursor {
    local stato=$1
    case $stato in
	off)
	    echo -e -n "\033[?30;30;30c"
	    ;;
	on)
	    echo -e -n "\033[?0;0;0c"
	    ;;
    esac
}

function header {
    ## $1=label ; $2=colors ; $3=header pattern

    # echo -n -e "\e[1;34m $1 ${Color_Off}\n"
    local text="$1"
    local length_text=$(( ${#text}+2 ))
    local hpattern="$3"
    
    [ -z "$hpattern" ] && hpattern=" "
    echo -ne "$2"

    for column in `seq 1 $COLUMNS`
    do
	echo -ne "$hpattern" 
    done 

    if [ -n "$length_text" ] &&
	   [ -n "$COLUMNS" ]
    then
	if (( $length_text<=$COLUMNS ))
	then
	    echo -ne "\r$text${Color_Off}\n"

	else
	    echo -ne "\r${text:0:$COLUMNS}${Color_Off}${text:$COLUMNS}\n"
	fi
    fi
}

function header_z {
    fclear
    zclock
    header " ZigzagProxy ($prog) $zclock" "$On_Blue" " "
}

function header_box {
    header " $1 " "$Black${On_White}" "─" #"-" 
}

function header_dl {
    header " $1 " "$White${On_Blue}" " "
}

function zclock {
    if [[ "${LANGUAGE}${LANG}" =~ (it|IT) ]]
    then
	week=( "dom" "lun" "mar" "mer" "gio" "ven" "sab" )

    else
	week=( "sun" "mon" "tue" "wed" "thu" "fri" "sat" )
    fi
    
    #zclock="\033[1;$((COLUMNS-22))f$(date +%R) │ ${week[$( date +%w )]} $(date +%d·%m·%Y)"
    zclock="$(date +%R) │ ${week[$( date +%w )]} $(date +%d·%m·%Y)"
}


########################################

if [[ "${LANGUAGE}${LANG}" =~ (it|IT) ]]
then
    msg[0]="Installazione di $PROG"
    msg[1]="Installazione di $PROG in /usr/local/bin/ ..."
    msg[2]="Installazione completata."
    msg[3]="Intallazione automatica non riuscita"
    
else
    msg[0]="Installing $PROG"
    msg[1]="Installing $PROG in /usr/local/bin/ ..."
    msg[2]="Installation completed"
    msg[3]="Installation failed"
fi

init_colors

pwd_old="$PWD"
header_box "${msg[0]}" #"Installazione di $PROG"
print_c 0 ""
cd /tmp

wget -T $max_waiting "$url_update"
    
print_c 4 "${msg[1]}" #"Installazione di $PROG in /usr/local/bin/ ..."
archive="${url_update##*\/}"
tar -xzf "$archive"

rm -f "$archive"

cd "${archive%.tar.gz}"
chmod +x $prog

version=$(wget -qO- http://download-mirror.savannah.gnu.org/releases/zproxy/version)

mv $prog /usr/local/bin/ &&
    echo "$version" >"$path_prog/version" &&
    print_c 1 "${msg[2]}" || #"Installazione completata."
	(
	    sudo mv $prog /usr/local/bin/ &&
		echo "$version" >"$path_prog/version" &&
		print_c 1 "${msg[2]}"
	) || #"Installazione completata." 
	(
	    echo -n "(Root)" &&
		su -c "mv $prog /usr/local/bin/" &&
		echo "$version" >"$path_prog/version" &&
		print_c 1 "${msg[2]}"
	) || #"Installazione completata." 
	print_c 3 "${msg[3]}" #"Intallazione automatica non riuscita"
echo
cd "$pwd_old"



 ZigzagProxy (ZPROXY)
======================
A Command Line Interface (CLI) to connect using proxies
(for Firefox, Chrome and any other program or command
started from bash)

Project (by zoninoz):
https://savannah.nongnu.org/projects/zproxy

Homepage:
https://www.inventati.org/zoninoz/html/index.php?module=text&aid=67

Releases:
http://download-mirror.savannah.gnu.org/releases/zproxy/


ZigzagProxy (zproxy) is a CLI bash script, with which
you can connect programs from bash using proxy
(transparent, anonymous or elite/highly-anonymous,
selected according to the type required by the user).

Zproxy extracts proxies from http://ip-adress.com/proxy_list/,
analyzes them and checks whether they work, selecting those
that allow a download speed higher than a default value



 INSTALLATION:
---------------
1) Download the installation script from:
   http://download-mirror.savannah.gnu.org/releases/zproxy/install_zproxy.sh

2) Give users the right to execute the script:
   chmod a+x install_zproxy.sh

3) Start the automatic installation:
   ./install_zproxy.sh
    


 Usage (the order of the arguments is not important):
---------------------------------------------------

	[source |.] zproxy [options] [commands | programs]


 [Source |.]:
--------------
You can activate/deactivate a proxy for all
programs/commands started from the same terminal:
'source' (abbreviated with a dot, must always be
inserted BEFORE the command) 'globalizes' the variables
'http_proxy' and 'https_proxy', making it active for all
commands/programs started from the terminal after ZigzagProxy.


 Options:
---------- 
-h      --help                ZigzagProxy Manual

        --ip                  Current IP address

-u      --update              Update ZigzagProxy

-d      --disable             Disable the proxy

-s      --ssl                 HTTPS proxy (ssl)

-t      --transparent         Select the type of automatic
-a      --anonymous           proxy to activate:
-e      --elite               Transparent, Anonymous, Elite

-p PROXY, -pPROXY             [-p|--proxy=] allows to activate
	--proxy=PROXY         a PROXY chosen by the user


 Commands or programs:
-----------------------
The program or commands to be started with active proxy.
In the presence of arguments or, in any case, of spaces,
it is necessary to enclose the command between 
quotes or single quotes.


 Examples:
-----------

- testing proxy HTTPS activation:
    zproxy --ip
    zproxy -s -t "wget -qO- https://indirizzo-ip.com/ip.php"

- activate proxy for Chromium (Transparent or Anonymous):
    zproxy -t -a chromium
  or:
    zproxy -ta chromium

- testing with Chromium:
    zproxy -t "chromium http://indirizzo-ip.com/ip.php"

- activate a proxy Transparent, Anonymous or Elite for
  all commands, scripts and programs started from the
  same terminal, after zproxy: 
    source zproxy -tae

- disable proxy and update zproxy:
    zproxy -du

- activate proxy "123.123.123.123:8080" for the terminal:
    . zproxy -p "123.123.123.123:8080"
  or:
    . zproxy -p"123.123.123.123:8080"
  or:
    source zproxy --proxy="123.123.123.123:8080"


 Note:
-------
ZigzagProxy can also activate a proxy
for Chrome/Chromium. For Firefox, verified
the availability and activability of a proxy,
we recommend the instructions below:
https://www.wikihow.com/Enter-Proxy-Settings-in-Firefox


 Copyright (C):
----------------
Gianluca Zoni <zoninoz@inventati.org>, 2012


 License:
----------
GNU/GPL v.3 or later: http://www.gnu.org/licenses/
